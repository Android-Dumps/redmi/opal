#!/system/bin/sh
array=($(echo "$2"))
for var in "${array[@]}"; do
   echo "$var"
   if [ -e /data/media/$var/Android/media/com.whatsapp ] && [ $var -gt 0 -a $var -lt 999 ] ; then
       echo user exists: $var
       chown -R $var$1:media_rw /data/media/$var/Android/media/com.whatsapp
   elif [ -e /data/media/$var/Android/media/com.whatsapp ] ; then
       echo user exists: $var
       chown -R $1:media_rw /data/media/$var/Android/media/com.whatsapp
   else
       echo $var does not exists
   fi
done